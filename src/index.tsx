import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { DIYDesignExperience } from './App';
import reportWebVitals from './reportWebVitals';

ReactDOM.render(
  <React.StrictMode>
    <DIYDesignExperience />
  </React.StrictMode>,
  document.getElementById('root')
);

reportWebVitals();
