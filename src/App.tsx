import { getSurfacesRequirements, getSurfaceUrl } from '@design-stack/cimdoc-design-lib/productsSdk';
import { DesignEngine } from '@design-stack/cimdoc-experiences-core/designEngine';
import {
  createPackagingDiyDesignEngine,
  PackagingDIYDesignExperience,
} from "@design-experiences/packaging-diy";
import { AuthConfiguration } from '@design-stack/utility/auth';

import { useEffect, useState } from 'react';
import { buildCimDocFromDesignRequirements, buildDesignRequirementsFromSurfaceSpecs, usePanelSections } from './utils'; // utility helper function to be implemented by consumer

const SKU = "VIP-45215";
const fontRepositoryUrl = "https://fonts.documents.cimpress.io/prod";
const modelConfigUrl = 'https://assets.documents.cimpress.io/v3/assets/26ca8d7f-4ab0-4217-8458-a9020990f28d/content';

const accessToken = ''; // Add token here

export function DIYDesignExperience() {

  // Provide access-Token
  const authConfig: AuthConfiguration = { authType: 'bearer', value: accessToken };
  const [designEngine, setDesignEngine] = useState<DesignEngine | null>(null);
  // const [sectionSet, setSectionSet] = useState<PanelSectionSet[] | null>(null);
  const { sectionSets } = usePanelSections({ modelConfigUrl: modelConfigUrl });
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    (async () => {
      const surfaceUrl = getSurfaceUrl(SKU);
      const surfaceData = await getSurfacesRequirements(surfaceUrl, accessToken);
      const designRequirements = buildDesignRequirementsFromSurfaceSpecs(surfaceData.surfaces);

      const cimDoc = buildCimDocFromDesignRequirements(SKU, designRequirements, fontRepositoryUrl);

      if (cimDoc && designRequirements) {
        const diyDesignEngine = createPackagingDiyDesignEngine({
          cimDoc: cimDoc,
          designRequirements: designRequirements,
          authConfig: authConfig,
        });
        setDesignEngine(diyDesignEngine);
        setLoading(false);
      }
    })()
  }, []);

  return (loading ? <span>Loading...</span> :
    designEngine && <div style={{ height: "100vh", width: "100vw" }}>
      <PackagingDIYDesignExperience
        designEngine={designEngine}
        nFoldConfig={{ modelConfigUrl, sectionSets }}
      />
    </div>
  );
}