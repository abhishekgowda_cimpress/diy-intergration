import { PanelSectionSet } from '@design-experiences/packaging-diy';
import { useEffect, useState } from 'react';
import axios from 'axios';


export function usePanelSections({ modelConfigUrl }: { modelConfigUrl: string | undefined }) {
    const [loading, setLoading] = useState(true);
    const [errorMessage, setErrorMessage] = useState<string | null>(null);
    const [sectionSets, setSectionSets] = useState<PanelSectionSet[] | null>(null);

    useEffect(() => {
        (async () => {
            setLoading(true);
            try {
                let sectionSetsPromise: Promise<PanelSectionSet[] | null>;
                if (modelConfigUrl) {
                    sectionSetsPromise = getPanelSections(modelConfigUrl);
                } else {
                    sectionSetsPromise = Promise.resolve(null);
                }
                const panelSectionSets = await sectionSetsPromise;

                setSectionSets(panelSectionSets);
                setLoading(false);
                setErrorMessage(null);
            } catch (error: any) {
                setLoading(false);
                setSectionSets(null);
                setErrorMessage(error.message);
            }
        })();
    }, [modelConfigUrl]);

    return {
        loading,
        errorMessage,
        sectionSets,
    };
}

async function getPanelSections(modelUrl: string): Promise<any> { // todo: update the type
    const modelData = await getPanelSectionsFromModelUrl(modelUrl).catch((error) => {
        throw new Error(`Failed to fetch model data: ${error.message}`);
    });

    const { sectionSets } = await getPanelSectionsFromProductDataUrl(modelData.productDataUrl).catch((error) => {
        throw new Error(`Failed to get panel sections information from productDataUrl: ${error.message}`);
    });

    return sectionSets;
}

function getPanelSectionsFromModelUrl(modelUrl: string) {
    return axios.get(modelUrl)
        .then((axiosResponse) => axiosResponse.data);
}

function getPanelSectionsFromProductDataUrl(productDataUrl: string) {
    return axios.get(productDataUrl)
        .then((axiosResponse) => axiosResponse.data);
}
