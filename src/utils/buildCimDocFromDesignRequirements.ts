
import { CimDoc } from '@design-stack/cimdoc-design-lib/types';
import { DesignRequirements } from '@design-stack/cimdoc-experiences-core/designEngine';

export function buildCimDocFromDesignRequirements(sku: string, designRequirements: DesignRequirements, fontRepositoryUrl: string) {
    const emptyCimDoc: CimDoc = {
        version: '2.0',
        document: {
            panels: [],
        },
        sku,
        fontRepositoryUrl: fontRepositoryUrl,
    };

    emptyCimDoc.document.panels = designRequirements.panels.map((panel) => ({
        id: panel.id,
        name: panel.name,
        width: panel.width,
        height: panel.height,
        decorationTechnology: panel.decorationTechnology,
    }));

    return emptyCimDoc;
}