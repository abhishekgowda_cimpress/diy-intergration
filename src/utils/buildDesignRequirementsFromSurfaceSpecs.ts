import { DesignRequirements } from '@design-stack/cimdoc-experiences-core/designEngine';
import { Guide, Mask } from '@design-stack/component-library/idaFramework';
import { Path, Surface } from '@design-stack/cimdoc-design-lib/productsSdk';


function mapPaths(paths: Path[]) {
    return paths.map((path) => ({
        anchor: {
            x: path.anchorX * 10,
            y: path.anchorY * 10,
        },
        points: path.pathPoints.map((pathPoint) => {
            const hasControlPoints =
                pathPoint.firstControlPointX != null &&
                pathPoint.secondControlPointX != null &&
                pathPoint.firstControlPointY != null &&
                pathPoint.secondControlPointY != null;
            return {
                x: pathPoint.endPointX * 10,
                y: pathPoint.endPointY * 10,
                controlPoints: hasControlPoints
                    ? {
                          first: {
                              x: pathPoint.firstControlPointX! * 10,
                              y: pathPoint.firstControlPointY! * 10,
                          },
                          second: {
                              x: pathPoint.secondControlPointX! * 10,
                              y: pathPoint.secondControlPointY! * 10,
                          },
                      }
                    : undefined,
            };
        }),
    }));
}


export function buildDesignRequirementsFromSurfaceSpecs(surfaces: Surface[]): DesignRequirements {
    return {
        panels: surfaces.map((surface) => {
            const masks: Mask[] =
                surface.masks.map((mask) => ({
                    type: mask.pathType,
                    paths: mapPaths(mask.paths),
                })) ?? [];

            const guides: Guide[] =
                surface.guides?.map((guide) => ({
                    type: guide.pathType,
                    paths: mapPaths(guide.paths),
                })) ?? [];

            return {
                id: surface.id,
                name: surface.name,
                width: `${surface.widthCm * 10}mm`,
                height: `${surface.heightCm * 10}mm`,
                decorationTechnology: surface.decorationTechnology,
                masks,
                guides,
            };
        }),
    };
}
